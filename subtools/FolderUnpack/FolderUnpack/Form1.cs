﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FolderUnpack
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Directory.Exists(_text_Folder.Text))
            {
                DirectoryInfo di = new DirectoryInfo(_text_Folder.Text);
                

                

                foreach (var processedDir in di.GetDirectories())
                {

                    if (processedDir.Parent == null || processedDir.FullName == processedDir.Parent.FullName)
                        break;

                    foreach (var file in processedDir.GetFiles())
                    {
                        var targetfilename = Path.Combine(di.FullName, file.Name);

                        if (File.Exists(targetfilename))
                            continue; //file exist in target folder
                        else
                        {
                            File.Move(file.FullName, targetfilename);
                        }
                    }

                    if (processedDir.IsEmpty())
                        Directory.Delete(processedDir.FullName);
                }

                System.Media.SystemSounds.Asterisk.Play();


            }
        }
    }

    public static class MyExtensions
    {
        public static bool IsEmpty(this DirectoryInfo dir)
        {
            return dir.GetFiles().Count() == 0 && dir.GetDirectories().Count() == 0;
        }
    }
}
