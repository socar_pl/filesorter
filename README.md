# FileSorter

.NET Framework application is a simple tool that integrates with Windows Explorer to sort files in a folder based on it's extension (or other parameters) and put every file to seprate folder, named after that extension (or other parameters - see below). Originally developed in 2010, moved from internal repo to gitlab with minor code changes and updates. 

If you have a folder(s) with mixed content that you would like quickly organize, this application will move files to seprate folders named after file extensions (ie. *.jpg). Currently application compiles to windows .NET 2.0 framework, with some minor updates/migration plans ahead. 
See following picture for a quick orientation:


**Download**

https://gitlab.com/socar_pl/filesorter/-/releases

**Features**
1. Sorting by:
  - File extension
  - First letter of the filename ("alphabetically")
  - Based on modification time 
  - Based on create time
2. Integration with Windows Explorer Folder context menu
3. Triggering default sorting (defined in application options) by passing parameter to the app. 
4. Ignoring files if similar file already exist in target directory 

**Changelog**
- version 2.3 - 2021.02.21 - added ignored extensions and custom prefix when sorting by extensions
- version 2.2 - circa 2014 - some code modifications for path support
- version 2.1 - 15.May.2010 - recovered from SVN repo and commited to Gitlab

**Plans**
- Move to .NET Core
- Redo GUI to some cross platform alternative

![FileSortQuickGuide](https://gitlab.com/socar_pl/filesorter/-/raw/master/FileSorter/Resources/filesort_quickguide.png)
