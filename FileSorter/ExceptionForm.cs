﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileSorter
{
    public partial class ExceptionForm : Form
    {
        public ExceptionForm(String text)
        {
            InitializeComponent();
            textBox1.Text = text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ExceptionForm_Load(object sender, EventArgs e)
        {

        }
    }
}
