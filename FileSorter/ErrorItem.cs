﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileSorter
{
    public class ErrorItem
    {
        public String FromPath { get; set; }
        public String ToPath { get; set; }
        public String Error { get; set; }
        public MoveFileResultStatus Msg { get; set; }
    }
}
