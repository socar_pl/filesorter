﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSorter
{
    public class AppConfig
    {
        public static AppConfig LoadConfig()
        {
            string json = File.ReadAllText(GetAppConfigPath());
            AppConfig ac = JsonConvert.DeserializeObject<AppConfig>(json);
            return ac;
        }

        public string GetIgnoredExtensionsAsCSV()
        {
            string returnedValue = "";
            if (this.IgnoredExtensions.Count>0)
            {
                returnedValue =  String.Join(";", this.IgnoredExtensions);
            }

            return returnedValue;
        }

        public static void SaveConfig(AppConfig config)
        {
            var configFile = File.CreateText(GetAppConfigPath());
            string json = JsonConvert.SerializeObject(config,Formatting.Indented);
            configFile.Write(json);
            configFile.Flush();
            configFile.Close();
        }

        private static string myAppConfig = "appconfig.json";
        public static string GetAppConfigPath()
        {
            return System.IO.Path.Combine(AppContext.BaseDirectory, myAppConfig);
        }

        public void Save()
        {
            AppConfig.SaveConfig(this);
        }

        public SortModeTypes GetSortingMethod()
        {
            SortModeTypes returnedValue = SortModeTypes.ExtensionSort;

            try
            {
                returnedValue = (SortModeTypes)this.DefaultSortingMethod;
            }
            catch 
            {
               
            }

            return SortModeTypes.ExtensionSort;
        }


        public int DefaultSortingMethod { get; set; }
        public bool CloseOnFinishedSort { get; set; }
        public string AppContextSetterApp { get; set; }
        public List<string> IgnoredExtensions { get; set; }
        public string SortByExtension_TargetFolderPrefix { get; set; }




    }
}
