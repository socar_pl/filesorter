﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileSorter
{
    public partial class AppPreferences : Form
    {
        public AppPreferences(AppConfig configToSet)
        {
            InitializeComponent();
            SetFormBasedOnConfig(configToSet);

        }

        private void SetFormBasedOnConfig(AppConfig configToSet)
        {
            if (configToSet == null)
            {
                _combo_DefaultSort.SelectedIndex = 0;
                _text_FolderNamePrefix.Text = ".";
            }
            else
            {
                _combo_DefaultSort.SelectedIndex = (int)configToSet.DefaultSortingMethod;
                _check_CloseOnFinishSort_IfNoErrors.Checked = configToSet.CloseOnFinishedSort;
                _text_FolderNamePrefix.Text = configToSet.SortByExtension_TargetFolderPrefix;
                _text_IgnoredFileExtensionCSV.Text = configToSet.GetIgnoredExtensionsAsCSV();
            }
        }

        public SortModeTypes GetSelectedSortType()
        {

            /*
             * File extension
First letter filename
Modification time
Creation time
             */
            String x = _combo_DefaultSort.SelectedItem.ToString();

            if (x == "File extension")
                return SortModeTypes.ExtensionSort;

            if (x == "First letter filename")
                return SortModeTypes.FileNameSort;

            if (x == "Modification time")
                return SortModeTypes.ModificationDateSort;

            if (x == "Creation time")
                return SortModeTypes.CreationDateSort;

            return SortModeTypes.ExtensionSort;

        }

        private void AppPreferences_Load(object sender, EventArgs e)
        {

        }

        private void _button_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        public AppConfig GetConfigFromForm()
        {
            AppConfig ret = new AppConfig();

            ret.DefaultSortingMethod = (int)this.GetSelectedSortType();
            ret.CloseOnFinishedSort = this._check_CloseOnFinishSort_IfNoErrors.Checked;
            ret.SortByExtension_TargetFolderPrefix = _text_FolderNamePrefix.Text;
            ret.IgnoredExtensions = GetIgnoredFileExtensions();
            return ret;
        }

        private List<string> GetIgnoredFileExtensions()
        {
            List<String> ret = new List<string>();
            if (!String.IsNullOrEmpty(_text_IgnoredFileExtensionCSV.Text))
            {
                string txt = _text_IgnoredFileExtensionCSV.Text;
                txt = txt.ToLower();
                var data = txt.Split(';');
                foreach (var item in data)
                {
                    string toAdd = item.Trim();
                    if (!String.IsNullOrEmpty(toAdd))
                        ret.Add(toAdd);
                }
            }

            return ret;
        }

        private void _button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void _button_AddExtension_Click_1(object sender, EventArgs e)
        {
            ShellManipulation.AddContextMenu();
        }

        private void _button_RemoveExtension_Click(object sender, EventArgs e)
        {
            ShellManipulation.RemoveContextMenu();
        }
    }
}
