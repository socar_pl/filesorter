﻿namespace FileSorter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this._progressBar_status = new System.Windows.Forms.ProgressBar();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this._text_FolderPath = new System.Windows.Forms.TextBox();
            this._button_Sort = new System.Windows.Forms.Button();
            this._check_Overwrite = new System.Windows.Forms.CheckBox();
            this._button_Options = new System.Windows.Forms.Button();
            this._button_About = new System.Windows.Forms.Button();
            this._button_SelectFolder = new System.Windows.Forms.Button();
            this._radio_Extension = new System.Windows.Forms.RadioButton();
            this._radio_FirstLetter = new System.Windows.Forms.RadioButton();
            this._radio_CreateTime = new System.Windows.Forms.RadioButton();
            this._radio_ModificationTime = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _progressBar_status
            // 
            this._progressBar_status.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._progressBar_status.Location = new System.Drawing.Point(12, 91);
            this._progressBar_status.Name = "_progressBar_status";
            this._progressBar_status.Size = new System.Drawing.Size(640, 23);
            this._progressBar_status.TabIndex = 2;
            // 
            // _text_FolderPath
            // 
            this._text_FolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._text_FolderPath.Location = new System.Drawing.Point(71, 13);
            this._text_FolderPath.Name = "_text_FolderPath";
            this._text_FolderPath.Size = new System.Drawing.Size(546, 20);
            this._text_FolderPath.TabIndex = 3;
            // 
            // _button_Sort
            // 
            this._button_Sort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._button_Sort.Location = new System.Drawing.Point(258, 62);
            this._button_Sort.Name = "_button_Sort";
            this._button_Sort.Size = new System.Drawing.Size(135, 23);
            this._button_Sort.TabIndex = 4;
            this._button_Sort.Text = "Sort files";
            this._button_Sort.UseVisualStyleBackColor = true;
            this._button_Sort.Click += new System.EventHandler(this._button_Sort_Click);
            // 
            // _check_Overwrite
            // 
            this._check_Overwrite.AutoSize = true;
            this._check_Overwrite.Location = new System.Drawing.Point(560, 40);
            this._check_Overwrite.Name = "_check_Overwrite";
            this._check_Overwrite.Size = new System.Drawing.Size(92, 17);
            this._check_Overwrite.TabIndex = 6;
            this._check_Overwrite.Text = "Overwrite files";
            this._check_Overwrite.UseVisualStyleBackColor = true;
            // 
            // _button_Options
            // 
            this._button_Options.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._button_Options.Image = global::FileSorter.Properties.Resources.MetroUI_Folder_OS_Configure_icon;
            this._button_Options.Location = new System.Drawing.Point(12, 120);
            this._button_Options.Name = "_button_Options";
            this._button_Options.Size = new System.Drawing.Size(32, 23);
            this._button_Options.TabIndex = 7;
            this._button_Options.UseVisualStyleBackColor = true;
            this._button_Options.Click += new System.EventHandler(this._button_Options_Click);
            // 
            // _button_About
            // 
            this._button_About.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._button_About.Image = global::FileSorter.Properties.Resources.Help_icon;
            this._button_About.Location = new System.Drawing.Point(620, 120);
            this._button_About.Name = "_button_About";
            this._button_About.Size = new System.Drawing.Size(32, 23);
            this._button_About.TabIndex = 7;
            this._button_About.UseVisualStyleBackColor = true;
            this._button_About.Click += new System.EventHandler(this._button_About_Click);
            // 
            // _button_SelectFolder
            // 
            this._button_SelectFolder.Image = global::FileSorter.Properties.Resources.folder_icon;
            this._button_SelectFolder.Location = new System.Drawing.Point(623, 11);
            this._button_SelectFolder.Name = "_button_SelectFolder";
            this._button_SelectFolder.Size = new System.Drawing.Size(29, 23);
            this._button_SelectFolder.TabIndex = 0;
            this._button_SelectFolder.UseVisualStyleBackColor = true;
            this._button_SelectFolder.Click += new System.EventHandler(this._button_SelectFolder_Click);
            // 
            // _radio_Extension
            // 
            this._radio_Extension.AutoSize = true;
            this._radio_Extension.Checked = true;
            this._radio_Extension.Location = new System.Drawing.Point(70, 39);
            this._radio_Extension.Name = "_radio_Extension";
            this._radio_Extension.Size = new System.Drawing.Size(71, 17);
            this._radio_Extension.TabIndex = 8;
            this._radio_Extension.TabStop = true;
            this._radio_Extension.Text = "Extension";
            this._radio_Extension.UseVisualStyleBackColor = true;
            // 
            // _radio_FirstLetter
            // 
            this._radio_FirstLetter.AutoSize = true;
            this._radio_FirstLetter.Location = new System.Drawing.Point(147, 39);
            this._radio_FirstLetter.Name = "_radio_FirstLetter";
            this._radio_FirstLetter.Size = new System.Drawing.Size(123, 17);
            this._radio_FirstLetter.TabIndex = 9;
            this._radio_FirstLetter.Text = "First letter in filename";
            this._radio_FirstLetter.UseVisualStyleBackColor = true;
            // 
            // _radio_CreateTime
            // 
            this._radio_CreateTime.AutoSize = true;
            this._radio_CreateTime.Location = new System.Drawing.Point(383, 39);
            this._radio_CreateTime.Name = "_radio_CreateTime";
            this._radio_CreateTime.Size = new System.Drawing.Size(78, 17);
            this._radio_CreateTime.TabIndex = 10;
            this._radio_CreateTime.Text = "Create time";
            this._radio_CreateTime.UseVisualStyleBackColor = true;
            // 
            // _radio_ModificationTime
            // 
            this._radio_ModificationTime.AutoSize = true;
            this._radio_ModificationTime.Location = new System.Drawing.Point(273, 40);
            this._radio_ModificationTime.Name = "_radio_ModificationTime";
            this._radio_ModificationTime.Size = new System.Drawing.Size(104, 17);
            this._radio_ModificationTime.TabIndex = 10;
            this._radio_ModificationTime.Text = "Modification time";
            this._radio_ModificationTime.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Folder path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Sort by:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 150);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._radio_Extension);
            this.Controls.Add(this._radio_FirstLetter);
            this.Controls.Add(this._radio_CreateTime);
            this.Controls.Add(this._radio_ModificationTime);
            this.Controls.Add(this._button_Options);
            this.Controls.Add(this._button_About);
            this.Controls.Add(this._check_Overwrite);
            this.Controls.Add(this._button_Sort);
            this.Controls.Add(this._text_FolderPath);
            this.Controls.Add(this._progressBar_status);
            this.Controls.Add(this._button_SelectFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "File sorter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _button_SelectFolder;
        private System.Windows.Forms.ProgressBar _progressBar_status;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox _text_FolderPath;
        private System.Windows.Forms.Button _button_Sort;
        private System.Windows.Forms.CheckBox _check_Overwrite;
        private System.Windows.Forms.Button _button_About;
        private System.Windows.Forms.Button _button_Options;
        private System.Windows.Forms.RadioButton _radio_Extension;
        private System.Windows.Forms.RadioButton _radio_FirstLetter;
        private System.Windows.Forms.RadioButton _radio_CreateTime;
        private System.Windows.Forms.RadioButton _radio_ModificationTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

