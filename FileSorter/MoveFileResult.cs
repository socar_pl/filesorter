﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSorter
{
    public class MoveFileResult
    {
        public static MoveFileResult Success()
        {
            return new MoveFileResult(MoveFileResultStatus.Success);
        }

        public MoveFileResult(MoveFileResultStatus status) : this(status, null) { }
        public MoveFileResult(MoveFileResultStatus status, Exception ex)
        {
            this.CatchedException = ex;
            this.Status = status;
        }

        public Exception CatchedException { get; set; }
        public MoveFileResultStatus Status { get; set; }

    }
}
