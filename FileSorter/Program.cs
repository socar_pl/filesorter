﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace FileSorter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Program.Config = AppConfig.LoadConfig();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            string[] parameters = Environment.GetCommandLineArgs();

            String dirToSort = "";
            if (parameters.Length > 1) 
               dirToSort = parameters[1];

            Application.Run(new Form1(dirToSort, Program.Config.GetSortingMethod()));
        }

        public static AppConfig Config { get; set; }

        public static string GetAppFolderPath {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static string GetVersionNumber(bool shortVersion=true)
        {
            string ver = "";
            if (shortVersion)
                ver = String.Format("{0}.{1}", Assembly.GetExecutingAssembly().GetName().Version.Major, Assembly.GetExecutingAssembly().GetName().Version.Minor);
            else
                ver = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            return ver;
        }


        public static string GetFileSorterAppPath {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().EscapedCodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                var nana = Path.GetFullPath(path);
                return nana;
         //       return codeBase;
            }
        }
    }
    public enum SortModeTypes : int { ExtensionSort = 0, FileNameSort = 1, ModificationDateSort = 2, CreationDateSort = 3 }
}
