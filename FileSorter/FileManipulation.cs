﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSorter
{
    class FileManipulation
    {
        public static MoveFileResult MoveFile(FileInfo fileToMove, bool AllowOverwrite, SortModeTypes sortingType)
        {
            if (fileToMove != null)
            {
                if (fileToMove.Exists)
                {

                    String targetFolderNameToMove = GetTargetFolderName(fileToMove, sortingType);

                    String fileDir = fileToMove.Directory.FullName;
                    String fileNewDir = Path.Combine(fileDir, targetFolderNameToMove);

                    String FilePath = fileToMove.FullName;
                    String NewFilePath = Path.Combine(fileNewDir, fileToMove.Name);

                    if (!Directory.Exists(fileNewDir))
                        if (!CreateDirectory(fileNewDir))
                            return new MoveFileResult(MoveFileResultStatus.CantCreateTargetFolder);


                    if (File.Exists(NewFilePath) && AllowOverwrite)
                    {
                        if (AllowOverwrite)
                        {
                            try
                            {
                                File.Delete(NewFilePath);
                            }
                            catch (Exception ex)
                            {
                                return new MoveFileResult(MoveFileResultStatus.TargetFileExist_Overwrite_FailedDeleteTargetFile, ex);
                            }
                        }
                        else
                            return new MoveFileResult(MoveFileResultStatus.TargetFileExist);
                    }


                    try
                    {
                        File.Move(FilePath, NewFilePath);
                    }
                    catch (Exception ex)
                    {
                        return new MoveFileResult(MoveFileResultStatus.Failed, ex);
                    }

                    return MoveFileResult.Success();

                }
                else
                {
                    return new MoveFileResult(MoveFileResultStatus.FileDoesNotExist);
                }
            }
            else
            {
                return new MoveFileResult(MoveFileResultStatus.FileInfoWasNull);
            }
        }

        public static string GetTargetFolderName(FileInfo fileToMove, SortModeTypes sortingType)
        {
            switch (sortingType)
            {
                case SortModeTypes.ExtensionSort:
                    return GetFolderNameForExtensionSorting(fileToMove.Extension);
                case SortModeTypes.CreationDateSort:
                    return fileToMove.CreationTime.ToString("yyyy-MM-dd");

                case SortModeTypes.ModificationDateSort:
                    return fileToMove.LastWriteTime.ToString("yyyy-MM-dd");

                case SortModeTypes.FileNameSort:
                    {
                        if (String.IsNullOrEmpty(fileToMove.Name))
                            return "no_letter";
                        else
                            return fileToMove.Name.First().ToString();
                    }
                default:
                    return fileToMove.Extension;
            }
        }

        public static string GetFolderNameForExtensionSorting(string extension)
        {

            if (String.IsNullOrEmpty(extension))
                return "_files_without_extensions";


            if (extension.StartsWith("."))
                extension = extension.Remove(0, 1);

            return Program.Config.SortByExtension_TargetFolderPrefix + extension;

        }

        public static bool CreateDirectory(string fileNewDir)
        {
            try
            {

                Directory.CreateDirectory(fileNewDir);

                if (Directory.Exists(fileNewDir))
                    return true;
            }
            catch
            {

            }
            return false;
        }

    }
}
