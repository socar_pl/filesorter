﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSorter
{
    public partial class Form1 : Form
    {
        public Form1(String DirectoryToSort = "", SortModeTypes sortType = SortModeTypes.ExtensionSort)
        {
            InitializeComponent();

            this.Text = "FileSorter ver " + Program.GetVersionNumber();
            _text_FolderPath.Text = DirectoryToSort;

            switch (sortType)
            {
                case SortModeTypes.CreationDateSort:
                    _radio_CreateTime.Checked = true; break;
                case SortModeTypes.FileNameSort:
                    _radio_FirstLetter.Checked = true; break;
                case SortModeTypes.ModificationDateSort:
                    _radio_ModificationTime.Checked = true; break;
                case SortModeTypes.ExtensionSort:
                default:
                    _radio_Extension.Checked = true; break;
            }
        }

        private void _button_SelectFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Directory.Exists(folderBrowserDialog1.SelectedPath))
                {
                    _text_FolderPath.Text = folderBrowserDialog1.SelectedPath;
                }
                else
                {
                    ShowError("Selected folder does not exist");
                }
            }
        }

        public void ShowError(String Message)
        {
            MessageBox.Show(Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public delegate int ReportProgressCallback(int p);
        public int ReportProgress(int Progress)
        {
            if (_progressBar_status.InvokeRequired)
            {
                _progressBar_status.Invoke(new ReportProgressCallback(ReportProgress), new object[] { Progress });
            }
            else
            {
                if (Progress > 100)
                    Progress = 100;
                _progressBar_status.Value = Progress;
            }

            return Progress;
        }


        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }



        public SortModeTypes SortMode
        {
            get
            {
                SortModeTypes mode = SortModeTypes.ExtensionSort;

                if (_radio_CreateTime.Checked)
                    mode = SortModeTypes.CreationDateSort;

                if (_radio_Extension.Checked)
                    mode = SortModeTypes.ExtensionSort;

                if (_radio_FirstLetter.Checked)
                    mode = SortModeTypes.FileNameSort;

                if (_radio_ModificationTime.Checked)
                    mode = SortModeTypes.ModificationDateSort;

                return mode;
            }
        }

        private void _button_Sort_Click(object sender, EventArgs e)
        {
            sortRunFromGUI = true;
            SortingCore.RunSort(_text_FolderPath.Text,
                ReportProgress,
                ReportSortResult,
                SortMode, _check_Overwrite.Checked
                );
        
        }

        bool sortRunFromGUI = false;



        public int ReportSortResult(SortResult result)
        {
            if(!result.Success)
            {
                if (result.ComplexErrorMessage)
                {
                    using (ExceptionForm ef = new ExceptionForm(result.ErrorMessage))
                    {
                        ef.ShowDialog();
                    }
                }
                else
                    ShowError(result.ErrorMessage);
            }
            else
            {
                if (Program.Config.CloseOnFinishedSort && !sortRunFromGUI)
                    Application.Exit();
            }

            return 0;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_text_FolderPath.Text))
            {
                  SortingCore.RunSort(
                    _text_FolderPath.Text,
                    ReportProgress,
                    ReportSortResult,
                    (SortModeTypes)Program.Config.DefaultSortingMethod);
            }
        }

        private void _button_About_Click(object sender, EventArgs e)
        {
            String text = @"FileSorter version {1}

Simple tool allowing to sort all files into folders named 
based on their extensions

© SOCAR {0}
https://www.socar.pl
";

            text = String.Format(text, DateTime.Now.Year, Program.GetVersionNumber(false));
            MessageBox.Show(text, "Help and About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void _button_Options_Click(object sender, EventArgs e)
        {
            using (AppPreferences ap = new AppPreferences(Program.Config))
            {
                if (ap.ShowDialog() == DialogResult.OK)
                {
                    Program.Config = ap.GetConfigFromForm();
                    Program.Config.Save();
                }

            }
        }




    }

    public enum MoveFileResultStatus { Success, FileInfoWasNull, FileDoesNotExist, Failed, TargetFileExist, TargetFileExist_Overwrite_FailedDeleteTargetFile, CantCreateTargetFolder };


}
