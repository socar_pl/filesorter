﻿namespace FileSorter
{
    partial class AppPreferences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppPreferences));
            this._button_OK = new System.Windows.Forms.Button();
            this._button_Cancel = new System.Windows.Forms.Button();
            this._check_CloseOnFinishSort_IfNoErrors = new System.Windows.Forms.CheckBox();
            this._combo_DefaultSort = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._label_IgnoredExtensions = new System.Windows.Forms.Label();
            this._text_IgnoredFileExtensionCSV = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this._text_FolderNamePrefix = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._button_AddExtension = new System.Windows.Forms.Button();
            this._button_RemoveExtension = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _button_OK
            // 
            this._button_OK.Location = new System.Drawing.Point(212, 313);
            this._button_OK.Name = "_button_OK";
            this._button_OK.Size = new System.Drawing.Size(55, 23);
            this._button_OK.TabIndex = 0;
            this._button_OK.Text = "OK";
            this._button_OK.UseVisualStyleBackColor = true;
            this._button_OK.Click += new System.EventHandler(this._button_OK_Click);
            // 
            // _button_Cancel
            // 
            this._button_Cancel.Location = new System.Drawing.Point(273, 313);
            this._button_Cancel.Name = "_button_Cancel";
            this._button_Cancel.Size = new System.Drawing.Size(55, 23);
            this._button_Cancel.TabIndex = 1;
            this._button_Cancel.Text = "Cancel";
            this._button_Cancel.UseVisualStyleBackColor = true;
            this._button_Cancel.Click += new System.EventHandler(this._button_Cancel_Click);
            // 
            // _check_CloseOnFinishSort_IfNoErrors
            // 
            this._check_CloseOnFinishSort_IfNoErrors.AutoSize = true;
            this._check_CloseOnFinishSort_IfNoErrors.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._check_CloseOnFinishSort_IfNoErrors.Location = new System.Drawing.Point(9, 82);
            this._check_CloseOnFinishSort_IfNoErrors.Name = "_check_CloseOnFinishSort_IfNoErrors";
            this._check_CloseOnFinishSort_IfNoErrors.Size = new System.Drawing.Size(163, 30);
            this._check_CloseOnFinishSort_IfNoErrors.TabIndex = 3;
            this._check_CloseOnFinishSort_IfNoErrors.Text = "When run from context menu\r\nclose on finish, if no errors";
            this._check_CloseOnFinishSort_IfNoErrors.UseVisualStyleBackColor = true;
            // 
            // _combo_DefaultSort
            // 
            this._combo_DefaultSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._combo_DefaultSort.FormattingEnabled = true;
            this._combo_DefaultSort.Items.AddRange(new object[] {
            "File extension",
            "First letter filename",
            "Modification time",
            "Creation time"});
            this._combo_DefaultSort.Location = new System.Drawing.Point(125, 16);
            this._combo_DefaultSort.Name = "_combo_DefaultSort";
            this._combo_DefaultSort.Size = new System.Drawing.Size(168, 21);
            this._combo_DefaultSort.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Default sorting type:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(316, 285);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._label_IgnoredExtensions);
            this.tabPage1.Controls.Add(this._text_IgnoredFileExtensionCSV);
            this.tabPage1.Controls.Add(this._combo_DefaultSort);
            this.tabPage1.Controls.Add(this._check_CloseOnFinishSort_IfNoErrors);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(308, 259);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "App Settings";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _label_IgnoredExtensions
            // 
            this._label_IgnoredExtensions.AutoSize = true;
            this._label_IgnoredExtensions.Location = new System.Drawing.Point(6, 50);
            this._label_IgnoredExtensions.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._label_IgnoredExtensions.Name = "_label_IgnoredExtensions";
            this._label_IgnoredExtensions.Size = new System.Drawing.Size(115, 13);
            this._label_IgnoredExtensions.TabIndex = 7;
            this._label_IgnoredExtensions.Text = "Ignored file extensions:";
            // 
            // _text_IgnoredFileExtensionCSV
            // 
            this._text_IgnoredFileExtensionCSV.Location = new System.Drawing.Point(125, 48);
            this._text_IgnoredFileExtensionCSV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._text_IgnoredFileExtensionCSV.Name = "_text_IgnoredFileExtensionCSV";
            this._text_IgnoredFileExtensionCSV.Size = new System.Drawing.Size(168, 20);
            this._text_IgnoredFileExtensionCSV.TabIndex = 6;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this._text_FolderNamePrefix);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Size = new System.Drawing.Size(308, 259);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Extension sorting";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(301, 93);
            this.label3.TabIndex = 2;
            this.label3.Text = resources.GetString("label3.Text");
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _text_FolderNamePrefix
            // 
            this._text_FolderNamePrefix.Location = new System.Drawing.Point(137, 21);
            this._text_FolderNamePrefix.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._text_FolderNamePrefix.Name = "_text_FolderNamePrefix";
            this._text_FolderNamePrefix.Size = new System.Drawing.Size(158, 20);
            this._text_FolderNamePrefix.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Folder name prefix:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._button_AddExtension);
            this.tabPage2.Controls.Add(this._button_RemoveExtension);
            this.tabPage2.Location = new System.Drawing.Point(4, 40);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(194, 144);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Explorer integration";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _button_AddExtension
            // 
            this._button_AddExtension.Location = new System.Drawing.Point(68, 36);
            this._button_AddExtension.Name = "_button_AddExtension";
            this._button_AddExtension.Size = new System.Drawing.Size(75, 23);
            this._button_AddExtension.TabIndex = 11;
            this._button_AddExtension.Text = "Add";
            this._button_AddExtension.UseVisualStyleBackColor = true;
            this._button_AddExtension.Click += new System.EventHandler(this._button_AddExtension_Click_1);
            // 
            // _button_RemoveExtension
            // 
            this._button_RemoveExtension.Location = new System.Drawing.Point(149, 36);
            this._button_RemoveExtension.Name = "_button_RemoveExtension";
            this._button_RemoveExtension.Size = new System.Drawing.Size(75, 23);
            this._button_RemoveExtension.TabIndex = 12;
            this._button_RemoveExtension.Text = "Remove";
            this._button_RemoveExtension.UseVisualStyleBackColor = true;
            this._button_RemoveExtension.Click += new System.EventHandler(this._button_RemoveExtension_Click);
            // 
            // AppPreferences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 346);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this._button_Cancel);
            this.Controls.Add(this._button_OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AppPreferences";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.AppPreferences_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _button_OK;
        private System.Windows.Forms.Button _button_Cancel;
        private System.Windows.Forms.CheckBox _check_CloseOnFinishSort_IfNoErrors;
        private System.Windows.Forms.ComboBox _combo_DefaultSort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button _button_AddExtension;
        private System.Windows.Forms.Button _button_RemoveExtension;
        private System.Windows.Forms.Label _label_IgnoredExtensions;
        private System.Windows.Forms.TextBox _text_IgnoredFileExtensionCSV;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _text_FolderNamePrefix;
        private System.Windows.Forms.Label label2;
    }
}