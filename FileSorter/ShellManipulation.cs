﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSorter
{
    public static class ShellManipulation
    {
        public static void AddContextMenu()
        {
            String path = Program.GetAppFolderPath;
            String exeSetter = Program.Config.AppContextSetterApp;

            String fileSorterApp = Program.GetFileSorterAppPath;

            string exec = Path.Combine(path, exeSetter);



            ProcessStartInfo psi = new ProcessStartInfo(exec, String.Format("{0} \"{1}\"", 1, fileSorterApp));//"1 " + fileSorterApp);
            Process.Start(psi);
        }

        public static void RemoveContextMenu()
        {
            String path = Program.GetAppFolderPath;
            String exeSetter = Program.Config.AppContextSetterApp;

            String fileSorterApp = Program.GetFileSorterAppPath;

            string exec = Path.Combine(path, exeSetter);



            ProcessStartInfo psi = new ProcessStartInfo(exec, "0");//"1 " + fileSorterApp);
            Process.Start(psi);
        }
    }
}
