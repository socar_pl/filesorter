﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSorter
{
    public static class SortingCore
    {
        public static void RunSort(
            String path,
            Func<int, int> ReportProgress = null,
            Func<SortResult, int> ReportSortResult = null,
            SortModeTypes sortingType = SortModeTypes.ExtensionSort, 
            bool overwrite = false
            )
        {
            //String path = _text_FolderPath.Text;
            //Boolean overwrite = config.overwrit

            if (!String.IsNullOrEmpty(path))
            {
                if (Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    Boolean ExceptionOccured = false;
                    List<string> IssueList = new List<string>();
                    Task.Factory.StartNew(() =>
                    {

                        var files = di.GetFiles();
                        int fileN = 0;
                        foreach (FileInfo fi in files)
                        {
                            fileN = fileN + 1;

                            if (ExtensionOnIgnoredList(fi))
                                continue;

                            var resultOfMove = FileManipulation.MoveFile(fi, overwrite, sortingType);

                            if (resultOfMove.Status != MoveFileResultStatus.Success)
                            {
                                ExceptionOccured = true;
                                IssueList.Add(String.Format("Error {0} for file {1}, Exception: {2}", resultOfMove.Status.ToString(), fi.FullName, resultOfMove.CatchedException.ToString()));
                            }
                            int progress = (int)((fileN * 100) / files.Length);

                            if (ReportProgress != null)
                                ReportProgress(progress);
                        }

                    }).ContinueWith((someArg) =>
                    {

                        SortResult sr = new SortResult();

                        if (!ExceptionOccured)
                        {
                            sr.Success = true;
                            
                        }
                        else
                        {
                                String add1 = String.Join(String.Format("{0}{1}{0}", Environment.NewLine, "-----------------"), IssueList);
                                String exceptionOccurredMessage = @"Sorting completed however following issues occured:
-----------------
" + add1;

                                sr.ErrorMessage = exceptionOccurredMessage;
                                sr.ComplexErrorMessage = true;
                                sr.Success = false;                                                          
                        }

                        if (ReportSortResult != null)
                            ReportSortResult(sr);

                    });

                }
                else if(ReportSortResult!=null)
                    ReportSortResult(new SortResult() { Success = false, ComplexErrorMessage = false, ErrorMessage = "Folder path does not exist" });
            }
            else if (ReportSortResult != null)
                ReportSortResult(new SortResult() { Success = false, ComplexErrorMessage = false, ErrorMessage = "Filepath is empty" });


            
        }

        private static bool ExtensionOnIgnoredList(FileInfo fi)
        {
            if (fi != null && !string.IsNullOrEmpty(fi.Extension))
            {
                string extL = fi.Extension.ToLower();
                return Program.Config.IgnoredExtensions.Contains(extL);
            }
            else
                return false;
        }
    }

    public class SortResult
    {
        private bool _complexErrorMessage = false;


        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public bool ComplexErrorMessage { get => _complexErrorMessage; set => _complexErrorMessage = value; }
    }
}
