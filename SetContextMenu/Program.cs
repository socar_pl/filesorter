using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;

namespace SetContextMenu
{
    class Program
    {
        private const string MenuName = "Folder\\shell\\SortUsingFileSorter";
        private const string Command = "Folder\\shell\\SortUsingFileSorter\\command";

        public static bool ContextMenuExistInReg
        {
            get
            {
                return false;
            }
        }

        public static void SetContextMenu(String path)
        {
            RegistryKey regmenu = null;
            RegistryKey regcmd = null;
            try
            {
                regmenu = Registry.ClassesRoot.CreateSubKey(MenuName);
                
                if (regmenu != null)
                    regmenu.SetValue("", "Sort using FileSorter");

                regcmd = Registry.ClassesRoot.CreateSubKey(Command);

                if (regcmd != null)
                    regcmd.SetValue("", path+" \"%1\"");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (regmenu != null)
                    regmenu.Close();
                if (regcmd != null)
                    regcmd.Close();
            }        
        }

        public static void RemoveContextMenu()
        {
            try
            {
                RegistryKey reg = Registry.ClassesRoot.OpenSubKey(Command);
                if (reg != null)
                {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(Command);
                }
                reg = Registry.ClassesRoot.OpenSubKey(MenuName);
                if (reg != null)
                {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(MenuName);
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
            }
        }

        // 1 c:\\app\\1.exe
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if(args[0].ToString().Equals("0"))
                    RemoveContextMenu();
                else if (args[0].ToString().Equals("1") && args.Length > 1)
                {
                    RemoveContextMenu();
                    SetContextMenu(args[1]);
                }
            }            
                
            
        }
    }
}
